package ru.pisarev.tm.enumerated;

import org.jetbrains.annotations.NotNull;

public enum Status {
    NOT_STARTED("Not started"),
    IN_PROGRESS("In progress"),
    COMPLETED("Complete");

    @NotNull
    private String displayName;

    @NotNull Status(@NotNull String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

}
