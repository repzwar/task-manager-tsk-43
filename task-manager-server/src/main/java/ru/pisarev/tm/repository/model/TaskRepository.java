package ru.pisarev.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.api.repository.model.ITaskRepository;
import ru.pisarev.tm.model.Task;

import javax.persistence.EntityManager;
import java.util.List;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    public TaskRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    public Task findById(@Nullable final String id) {
        return entityManager.find(Task.class, id);
    }

    public Task getReference(@NotNull final String id) {
        return entityManager.getReference(Task.class, id);
    }

    public void removeById(@Nullable final String id) {
        Task reference = entityManager.getReference(Task.class, id);
        entityManager.remove(reference);
    }

    @NotNull
    public List<Task> findAll() {
        return entityManager.createQuery("SELECT e FROM Task e", Task.class).getResultList();
    }

    @Override
    public List<Task> findAllByUserId(String userId) {
        return entityManager
                .createQuery("SELECT e FROM Task e WHERE e.user.id = :userId", Task.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public List<Task> findAllTaskByProjectId(String userId, String projectId) {
        return entityManager
                .createQuery(
                        "SELECT e FROM Task e WHERE e.user.id = :userId AND e.project.id = :projectId", Task.class
                )
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Override
    public void removeAllTaskByProjectId(String userId, String projectId) {
        entityManager
                .createQuery("DELETE FROM Task e WHERE e.user.id = :userId AND e.project.id = :projectId")
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .executeUpdate();
    }

    @Override
    public void bindTaskToProjectById(String userId, String taskId, String projectId) {
        entityManager
                .createQuery("UPDATE Task e SET e.project.id = :projectId WHERE e.user.id AND e.id = :id")
                .setParameter("userId", userId)
                .setParameter("id", taskId)
                .setParameter("projectId", projectId)
                .executeUpdate();
    }

    @Override
    public void unbindTaskById(String userId, String id) {
        entityManager
                .createQuery("UPDATE Task e SET e.project.id = NULL WHERE e.user.id AND e.id = :id")
                .setParameter("userId", userId)
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public Task findByIdUserId(String userId, String id) {
        return getSingleResult(
                entityManager
                        .createQuery("SELECT e FROM Task e WHERE e.id = :id AND e.user.id = :userId", Task.class)
                        .setParameter("id", id)
                        .setParameter("userId", userId)
                        .setMaxResults(1)
        );
    }


    @Nullable
    @Override
    public Task findByName(@NotNull final String userId, @Nullable final String name) {
        return getSingleResult(
                entityManager
                        .createQuery("SELECT e FROM Task e WHERE e.name = :name AND e.user.id = :userId", Task.class)
                        .setParameter("name", name)
                        .setParameter("userId", userId)
                        .setMaxResults(1)
        );
    }

    @Nullable
    @Override
    public Task findByIndex(@NotNull final String userId, final int index) {
        return getSingleResult(
                entityManager
                        .createQuery("SELECT e FROM Task e WHERE e.user.id = :userId", Task.class)
                        .setParameter("userId", userId)
                        .setFirstResult(index)
                        .setMaxResults(1)
        );
    }

    @Override
    public void removeByName(@NotNull final String userId, @Nullable final String name) {
        entityManager
                .createQuery("DELETE FROM Task e WHERE e.name = :name AND e.user.id = :userId")
                .setParameter("userId", userId)
                .setParameter("name", name)
                .executeUpdate();
    }

    @Override
    public void removeByIndex(@NotNull final String userId, final int index) {
        entityManager
                .createQuery("DELETE FROM Task e WHERE e.user.id = :userId")
                .setParameter("userId", userId)
                .setFirstResult(index).setMaxResults(1).executeUpdate();
    }

    public void clear() {
        entityManager
                .createQuery("DELETE FROM Task e")
                .executeUpdate();
    }

    @Override
    public void clearByUserId(String userId) {
        entityManager
                .createQuery("DELETE FROM Task e WHERE e.user.id = :userId")
                .setParameter("userId", userId).executeUpdate();
    }

    @Override
    public void removeByIdUserId(String userId, String id) {
        entityManager
                .createQuery("DELETE FROM Task e WHERE e.user.id = :userId AND e.id = :id")
                .setParameter("id", id)
                .setParameter("userId", userId)
                .executeUpdate();
    }

}